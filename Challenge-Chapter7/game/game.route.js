const express = require('express');
const gameRouter = express.Router();
const gameController = require("./game.controller");
const authorization = require("../middleware/authMiddleware");
const protection = require("../middleware/protectMiddleware");
const gameSchema = require("./game.schema");

//API to create new room
gameRouter.post(
    "/create-room",
    authorization,
    gameSchema.recordPlayerOneSchema(),
    gameController.createNewRoom
);

//API to get all rooms
gameRouter.get(
    "/rooms",
    authorization,
    gameController.getAllRooms
);

//API to get detail room
gameRouter.get(
    "/rooms/:idRoom",
    authorization,
    gameController.getDetailRoom
);

//API to update detail room
gameRouter.put(
    "/rooms/:idRoom",
    authorization,
    gameSchema.recordPlayerTwoSchema(),
    gameController.updateDetailRoom
);

//API to record history game by id
//record game history berada di API create room & update room
// gameRouter.put(
//     "/history",
//     authorization,
//     gameController.recordGameHistory
// );

//API to get game history by id
gameRouter.get(
    "/history/:idGame",
    authorization,
    gameController.getGameHistory
);

module.exports = gameRouter;