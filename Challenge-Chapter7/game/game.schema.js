const { checkSchema } = require("express-validator")

class GameSchema {

    recordPlayerOneSchema = () => {
        return checkSchema({
            roomName: {
                notEmpty: true,
                isLength: { options: { max: 15 } },
                errorMessage: "room name maximal 15 character"
            },
            playerOneChoice: {
                notEmpty: true,
                isIn:  {
                options: [['rock', 'paper', 'scissors']]
            },
            errorMessage: "your choice must be rock/paper/scissor"
            }
        });
    };

    recordPlayerTwoSchema = () => {
        return checkSchema({
            playerTwoChoice: {
                notEmpty: true,
                isIn:  {
                options: [['rock', 'paper', 'scissors']]
            },
            errorMessage: "your choice must be rock/paper/scissor"
            }
        });
    };

};


module.exports = new GameSchema();