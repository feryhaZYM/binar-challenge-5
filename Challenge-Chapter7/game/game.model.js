const database = require("../db/models");

class GameModel {

    createNewRoom = async (dataPlayer1, tokenId) => {
        return await database.GameRoom.create({
            roomName: dataPlayer1.roomName,
            playerOneId: tokenId,
            playerOneChoice: dataPlayer1.playerOneChoice
        });
    };

    isRoomNameUsed = async (dataPlayer1) => {
        const nameOfRoom = await database.GameRoom.findOne({
            where: { roomName: dataPlayer1.roomName }
        });
        if (nameOfRoom) {
            return true;
        } else {
            return false;
        }
    };

    getAllRooms = async () => {
        const allRooms = await database.GameRoom.findAll({
            raw: true,
            attributes: {
                exclude:
                    [
                        "playerOneId", "playerOneChoice", "resultP1",
                        "playerTwoId", "playerTwoChoice", "resultP2",
                        "createdAt", "updatedAt"
                    ]
            },
            include: [
                {
                    model: database.User,
                    as: "player1",
                    attributes: { exclude: ["id", "password", "email", "createdAt", "updatedAt"] },
                },
                {
                    model: database.User,
                    as: "player2",
                    attributes: { exclude: ["id", "password", "email", "createdAt", "updatedAt"] }
                }
            ]
        });
        return allRooms;
    };

    getDetailRoom = async (idRoom) => {
        const roomDetail = await database.GameRoom.findOne({
            attributes: {
                exclude:
                    [
                        "playerOneId", "resultP1", "playerTwoId", "resultP2", "createdAt", "updatedAt"
                    ]
            },
            raw: true,
            where: { id: idRoom },
            include: [
                {
                    model: database.User,
                    as: "player1",
                    attributes: { exclude: ["id", "password", "email", "createdAt", "updatedAt"] },
                },
                {
                    model: database.User,
                    as: "player2",
                    attributes: { exclude: ["id", "password", "email", "createdAt", "updatedAt"] }
                }
            ]
        });
        return roomDetail;
    };

    updateDetailRoom = async (idRoom, playerTwoChoice, tokenId) => {
        return await database.GameRoom.update(
            {
                playerTwoId: tokenId,
                playerTwoChoice: playerTwoChoice,
            },
            { where: { id: idRoom } }
        );
    };

    getRoomById = async (idRoom) => {
        return await database.GameRoom.findOne({
            where: { id: idRoom }
        });
    };

    findPlayerChoices = async (idRoom) => {
        return await database.GameRoom.findOne({
            raw: true,
            attributes: {
                exclude:
                    [
                        "id", "roomName", "playerOneId", "resultP1", "playerTwoId", "resultP2", "createdAt", "updatedAt"
                    ]
            },
            where: { id: idRoom }
        });
    };

    updateResultPlayerDraw = async (idRoom, draw) => {
        return await database.GameRoom.update(
            {
                resultP1: draw,
                resultP2: draw
            },
            { where: { id: idRoom } }
        );
    };

    updateResultPlayer1Win = async (idRoom, lose, win) => {
        return await database.GameRoom.update(
            {
                resultP1: win,
                resultP2: lose
            },
            { where: { id: idRoom } }
        );
    };

    updateResultPlayer2Win = async (idRoom, win, lose) => {
        return await database.GameRoom.update(
            {
                resultP1: lose,
                resultP2: win
            },
            { where: { id: idRoom } }
        );
    };

    recordGameHistory = async (tokenId, idRoom) => {
        return await database.GameHistory.create({
            userId: tokenId,
            roomId: idRoom
        });
    };

    getGameHistory = async (idGame) => {
        const histories = await database.GameHistory.findOne({
            where: { userId: idGame },
        });
        return histories;
    };

    getGameHistoryPlayer1 = async (idGame) => {
        const histories = await database.GameRoom.findAll({
            attributes: {
                exclude: ["id", "playerTwoId", "resultP2", "createdAt", "updatedAt"]
            },
            where: { playerOneId: idGame },
            include: [
                {
                    model: database.GameHistory,
                    attributes: {
                        exclude: ["id", "createdAt",]
                    }
                }
            ]
        });
        return histories;
    };

    getGameHistoryPlayer2 = async (idGame) => {
        const histories = await database.GameRoom.findAll({
            attributes: {
                exclude: ["id", "playerOneId", "resultP1", "createdAt", "updatedAt"]
            },
            where: { playerTwoId: idGame },
            include: [
                {
                    model: database.GameHistory,
                    attributes: {
                        exclude: ["id", "createdAt",]
                    }
                }
            ]
        });
        return histories;
    };

    // getGameHistoryPlayer1 = async (idGame) => {
    //     const histories = await database.GameHistory.findAll({
    //         where: { userId: idGame },
    //         include: [
    //             {
    //                 model: database.GameRoom,
    //                 attributes: {
    //                     exclude: ["id", "playerTwoId", "resultP2", "createdAt", "updatedAt"]
    //                 }
    //             }
    //         ]
    //     });
    //     return histories;
    // };

    // getGameHistoryPlayer2 = async (idGame) => {
    //     const histories = await database.GameHistory.findAll({
    //         where: { userId: idGame },
    //         include: [
    //             {
    //                 model: database.GameRoom,
    //                 attributes: {
    //                     exclude: ["id", "playerOneId", "resultP1", "createdAt", "updatedAt"]
    //                 }
    //             }
    //         ]
    //     });
    //     return histories;
    // };

};

module.exports = new GameModel();