const { validationResult } = require("express-validator");
const gameModel = require("./game.model");

class GameController {

    createNewRoom = async (req, res) => {
        const dataPlayer1 = req.body;
        const tokenId = req.token.id;
        const result = validationResult(req);
        console.log(result);

        if (!result.isEmpty()) {
            res.statusCode = 400;
            return res.json({ errors: result.errors });
        } else {
            const nameOfRoom = await gameModel.isRoomNameUsed(dataPlayer1);
            
            if (nameOfRoom) {
                res.statusCode = 409;
                return res.json({ message: "The room name is already used, please use a another name" });
            }
            const roomCreated = await gameModel.createNewRoom(dataPlayer1, tokenId);
            const idRoom = roomCreated.id
            console.log(roomCreated.id);
            if (roomCreated) {
                await gameModel.recordGameHistory(tokenId, idRoom);
                res.statusCode = 201;
                return res.json({ message: "success create new Room" })
            }
        }
    };

    getAllRooms = async (req, res) => {
        const rooms = await gameModel.getAllRooms();
        if (rooms) {
            return res.json(rooms);
        }
    };

    getDetailRoom = async (req, res) => {
        const { idRoom } = req.params;
        try {
            const roomDetail = await gameModel.getDetailRoom(idRoom);
            if (roomDetail) {
                return res.json(roomDetail);
            } else {
                res.statusCode = 404;
                return res.json({ message: `room with id: ${idRoom} not available` })
            }
        } catch (error) {
            res.statusCode = 500;
            return res.json({ message: "error" })
        }
    };

    updateDetailRoom = async (req, res) => {
        const { idRoom } = req.params;
        const { playerTwoChoice } = req.body;
        const tokenId = req.token.id;
        const result = validationResult(req);
        console.log(result);

        if (!result.isEmpty()) {
            res.statusCode = 400;
            return res.json({ errors: result.errors });
        } else {
            const updatedData = await gameModel.updateDetailRoom(idRoom, playerTwoChoice, tokenId);
            const detailRoom = await gameModel.getRoomById(idRoom);

            if (detailRoom.playerOneId === detailRoom.playerTwoId) {
                res.statusCode = 403;
                return res.json({ message: "you cannot playing with yourself!" })
            } else if (detailRoom.resultP1 !== null && detailRoom.resultP1 !== null) {
                res.statusCode = 403;
                return res.json({ message: "The Game can only be played once!" })
            } else if (updatedData) {
                const recordedHistory = await gameModel.recordGameHistory(tokenId, idRoom);
                const player1 = detailRoom.playerOneChoice;
                const player2 = detailRoom.playerTwoChoice;
                const draw = "DRAW";
                const win = "WIN";
                const lose = "LOSE";

                if (
                    (player1 === "rock" && player2 === "rock") ||
                    (player1 === "paper" && player2 === "paper") ||
                    (player1 === "scissors" && player2 === "scissors")
                ) {
                    res.statusCode = 202;
                    res.json({ message: "DRAW" });
                    return await gameModel.updateResultPlayerDraw(idRoom, draw);
                } else if (
                    (player1 === "rock" && player2 === "paper") ||
                    (player1 === "paper" && player2 === "scissors") ||
                    (player1 === "scissors" && player2 === "rock")
                ) {
                    res.statusCode = 202;
                    res.json({ message: "YOU WIN" });
                    return await gameModel.updateResultPlayer2Win(idRoom, win, lose);
                } else if (
                    (player1 === "rock" && player2 === "scissors") ||
                    (player1 === "paper" && player2 === "rock") ||
                    (player1 === "scissors" && player2 === "paper")
                ) {
                    res.statusCode = 202;
                    res.json({ message: "YOU LOSE" });
                    return await gameModel.updateResultPlayer1Win(idRoom, lose, win);
                }
                return recordedHistory;
            };
        };
    };

    // recordGameHistory = async (req, res) => {
    //     //record game history berada di API create room & update room
    // };

    //MASIH ERROR BELUM FIX HEHEHE
    getGameHistory = async (req, res) => {
        const { idGame } = req.params;

        try {
            const histories = await gameModel.getGameHistory(idGame);
            const idRoom = histories.roomId;
            const detailRoom = await gameModel.getRoomById(idRoom);
            const playerOneId = detailRoom.playerOneId.toString();
            const playerTwoId = detailRoom.playerTwoId.toString();
            // console.log(detailRoom.playerOneId);
            // console.log(detailRoom.playerTwoId);
            if (idGame === playerOneId) {
                const player1 = await gameModel.getGameHistoryPlayer1(idGame);
                return res.json(player1);
            } else if (idGame === playerTwoId) {
                const player2 = await gameModel.getGameHistoryPlayer2(idGame);
                return res.json(player2);
            } else {
                res.statusCode = 404;
                return res.json({ message: `user with id: ${idGame} has no game history` })
            }
        } catch (error) {
            res.statusCode = 500;
            console.log(typeof error)
            return res.json({ message: "error" })
        }
    };

};

module.exports = new GameController();