const express = require("express");
const app = express();
const port = 5000;
const usersRouter = require("./users/users.route");
const gameRouter = require("./game/game.route");
const cors = require("cors");
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("./swagger.json");
require("dotenv").config();

//middleware
app.use(cors());
app.use(express.json());

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use("/users", usersRouter);
app.use("/game", gameRouter);

app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
