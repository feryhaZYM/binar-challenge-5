const md5 = require("md5");
const database = require("../db/models");
const { Op } = require("sequelize");

class UsersModel {

    isUsernameRegistered = async (requestData) => {
        const username = await database.User.findOne({
            where: {username: requestData.username}
        });
        //SELECT username FROM "Users";
        if (username) {
            return true;
        } else {
            return false;
        }
    };

    isEmailRegistered = async (requestData) => {
        const email = await database.User.findOne({
            where: {email: requestData.email}
        });
        //SELECT email FROM "Users";
        if (email) {
            return true;
        } else {
            return false;
        }
    };

    recordNewData = (requestData) => {
        database.User.create({
            username: requestData.username,
            email: requestData.email,
            password: md5(requestData.password)
        });
    };

    findUserByUsername = async (username) => {
        const usernameQuery = await database.User.findOne({
            where: {username: username}
        });
        //SELECT username FROM "Users";
        return usernameQuery;
    };

    getAllDataUser = async () => {
        const dataUser = await database.User.findAll({
            include: [database.UserBiodata]
        });
        //SELECT * FROM "Users";
        return dataUser;
    };

    verifyLogin = async (email, password) => {
        const dataLogin = await database.User.findOne({
            raw: true,
            attributes: {exclude: ["password", "email"]},
            where: {
                [Op.and]: [
                    {email: email},
                    {password: md5(password)}
                ]
            },
        });
        //SELECT email, password FROM "Users";
        return dataLogin;
    };

    verifyEmail = async (email) => {
        const dataEmail = await database.User.findOne({
            where: {email: email}
        });
        //SELECT email FROM "Users";
        return dataEmail;
    };

    verifyPassword = async (password) => {
        const dataPassword = await database.User.findOne({
            where: {password: password}
        });
        //SELECT email FROM "Users";
        return dataPassword;
    };
    
    getBiodata = async (idBio) => {
        return await database.UserBiodata.findOne({
            include: [database.User],
            where: {userId: idBio}
        });
    };

    isBiodataAvailable = async (idBio) => {
        const availableBio = await database.UserBiodata.findOne({
            where: { userId: idBio }
        });
        
        if(availableBio){
            return true;
        } else {
            return false;
        }
    };

    createBiodata = async (idBio, biodata) => {
        const createdBio = await database.UserBiodata.create(
            {
                fullname: biodata.fullname,
                phone: biodata.phone,
                birth: biodata.birth,
                address: biodata.address,
                userId: idBio
            },
            { where: {userId: idBio}}
        );
        return createdBio;
    };
    
    updateBiodata = async (idBio, biodata) => {
        const updatedBio = await database.UserBiodata.update(
            {
                fullname: biodata.fullname,
                phone: biodata.phone,
                birth: biodata.birth,
                address: biodata.address,
                userId: idBio
            },
            { where: {userId: idBio}}
        );
        return updatedBio;
    };

    
};

module.exports = new UsersModel();