const { checkSchema } = require("express-validator");

class UsersSchema {
    
    registrationSchema = () => {
        return checkSchema({
            username: {
                notEmpty: true,
                errorMessage: "Enter username!",
            },
            email: {
                notEmpty: true,
                errorMessage: "Enter email!",
                isEmail: true,
                errorMessage: "email is invalid, must use the correct email format!",
            },
            password: {
                notEmpty: true,
                errorMessage: "Enter password!",
                isLength: {
                    options: { min: 8 },
                    errorMessage: "Password should be at least 8 character!",
                },
            },
        });
    };

    loginSchema = () => {
        return checkSchema({
            email: {
                notEmpty: true,
                errorMessage: "Enter your email!",
                isEmail: true,
                errorMessage: "email is invalid, must use the correct email format!",
            },
            password: {
                notEmpty: true,
                errorMessage: "Enter your password!",
            }
        });
    };

    updateBioSchema = () => {
        return checkSchema({
            fullname: {
                notEmpty: true,
                errorMessage: "input your fullname without number"
            },
            phone: {
                isMobilePhone: {locale: "id-ID"},
                errorMessage: "input your phone number without letters"
            },
            birth: {
                isDate: true,
                errorMessage: "input your birth date with format YYYY/MM/DD"
            },
            address: {
                notEmpty: true,
                errorMessage: "input your address"
            }
        });
    };

    
};

module.exports = new UsersSchema();

