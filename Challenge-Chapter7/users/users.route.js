const express = require("express");
const userRouter = express.Router();
const usersController = require("./users.controller");
const authorization = require("../middleware/authMiddleware");
const protection = require("../middleware/protectMiddleware");
const usersSchema = require("./users.schema");

//API to registration new user
userRouter.post(
    "/sign-up",
    usersSchema.registrationSchema(),
    usersController.userRegister);

//API to login
userRouter.post(
    "/sign-in",
    usersSchema.loginSchema(),
    usersController.userLogin);

//API to get data
userRouter.get(
    "/data-user", 
    authorization, 
    usersController.getUser
);

//API to get biodata by Id
userRouter.get(
    "/biodata/:idBio", 
    authorization, 
    usersController.getBiodata
);

//API to update or create biodata by Id
userRouter.put(
    "/biodata/:idBio",
    authorization,
    protection,
    usersSchema.updateBioSchema(),
    usersController.updateBiodata
);

module.exports = userRouter;