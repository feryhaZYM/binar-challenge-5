const usersModel = require("./users.model");
const jwt = require("jsonwebtoken");
const { validationResult } = require("express-validator");
require("dotenv").config();

class UsersController {

    userRegister = async (req, res) => {
        //validation input data
        const requestData = req.body;
        const result = validationResult(req);
        // console.log(requestData);
        // console.log(result);

        if (!result.isEmpty()) {
            res.statusCode = 400;
            return res.json({ errors: result.errors });
        } else {
            const username = await usersModel.isUsernameRegistered(requestData);
            const email = await usersModel.isEmailRegistered(requestData);
            //varify useername is registered or not
            if (username && email) {
                res.statusCode = 409;
                return res.json({ message: "Username and Email is Registered!" });
            } else if (username) {
                res.statusCode = 409;
                return res.json({ message: "Username is Registered!" });
            } else if (email) {
                res.statusCode = 409;
                return res.json({ message: "Email is Registered!" });
            }
        }
        //record new data
        usersModel.recordNewData(requestData);
        res.statusCode = 201;
        res.json({ message: "success registration" });
    };

    getUser = async (req, res) => {
        const { username } = req.query;
        const dataUser = await usersModel.getAllDataUser();
        //get data user by request query username => /users?username=...
        if (username) {
            const usernameQuery = await usersModel.findUserByUsername(username);
            if (usernameQuery === undefined || usernameQuery === "" || usernameQuery === null) {
                res.statusCode = 404;
                return res.json({ message: "Username doesn't exist!" });
            } else if (usernameQuery) {
                res.json(usernameQuery);
            }
            //get all data
        } else if (dataUser) {
            res.json(dataUser);
        } else {
            res.statusCode = 500;
            res.json({ message: "error!" })
        }
    };

    userLogin = async (req, res) => {
        const { email, password } = req.body;
        const result = validationResult(req);
        console.log(result.errors)
        try {
            const dataLogin = await usersModel.verifyLogin(email, password);
            const dataEmail = await usersModel.verifyEmail(email);
            const dataPassword = await usersModel.verifyPassword(password);

            if (!result.isEmpty()) {
                res.statusCode = 400;
                return res.json({ errors: result.errors });
            } else {
                if (dataLogin) {
                    const token = jwt.sign(
                        { ...dataLogin, role: "player" },
                        process.env.SECRET_KEY,
                        { expiresIn: "1d" }
                    );
                    return res.json({ accessToken: token });
                } else if (!dataPassword && dataEmail) {
                    res.statusCode = 401;
                    return res.json({ message: "The password you entered is incorrect!" });
                } else if (!dataLogin && !dataEmail) {
                    res.statusCode = 404;
                    return res.json({ meesage: "cannot found user!" });
                } else {
                    res.statusCode = 404;
                    return res.json({ message: "user not found" });
                }
            }
        } catch (error) {
            res.statusCode = 500;
            res.json({ message: "server error!" });
        }
    };

    getBiodata = async (req, res) => {
        const { idBio } = req.params;
        try {
            const biodata = await usersModel.getBiodata(idBio);
            if (biodata) {
                return res.json(biodata);
            } else {
                res.statusCode = 404;
                return res.json({ message: `biodata user with id: ${idBio} not found!` });
            }
        } catch (error) {
            res.statusCode = 400;
            return res.json({ message: "request error!" })
        }
    };

    updateBiodata = async (req, res) => {
        const { idBio } = req.params;
        const biodata = req.body;
        const availableBio = await usersModel.isBiodataAvailable(idBio);
        const result = validationResult(req);
        // console.log(result);

        if (!result.isEmpty()) {
            res.statusCode = 400;
            return res.json({ errors: result.errors });
        } else {
            if (availableBio) {
                const updatedBio = await usersModel.updateBiodata(idBio, biodata);
                res.statusCode = 202;
                res.json({ message: "success update biodata" });
                return updatedBio;
            } else if (!availableBio) {
                const createdBio = await usersModel.createBiodata(idBio, biodata);
                res.statusCode = 201;
                res.json({ message: "success create biodata" });
                return createdBio;
            }
        }
    };
};


module.exports = new UsersController();