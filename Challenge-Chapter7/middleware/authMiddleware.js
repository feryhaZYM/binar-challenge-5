const jwt = require("jsonwebtoken");
require("dotenv").config();

const authorization = (req, res, next) => {
    const { authorization } = req.headers;

    if (authorization === undefined) {
        res.statusCode = 401;
        return res.json({ message: "Unauthorized" });
    } else {
        try {
            const splitedToken = authorization.split(" ")[1];
            const token = jwt.verify(
                splitedToken,
                process.env.SECRET_KEY
            );
            req.token = token;
            next();
        } catch (error) {
            res.statusCode = 400;
            return res.json({ message: "token is invalid" })
        };
    };

};

module.exports = authorization;