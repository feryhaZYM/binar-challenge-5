protection = (req, res, next) => {
    const { idBio, idGame } = req.params;
    const idToken = req.token.id.toString();

    // console.log(`idToken=> ${req.token.id}`);
    // console.log(`idBio=> ${idBio}`);
    // console.log(`idGame=> ${idGame}`);
    // console.log(Buffer.from(req.headers.authorization.toString(), 'base64').toString('ascii'));

    if ((idToken !== idBio && idGame === undefined) || (idToken !== idGame && idBio === undefined)) {
        res.statusCode = 403;
        res.json({ message: "request forbidden" });
    } else if (idToken === idBio || idToken === idGame) {
        next();
    }

};

module.exports = protection;