'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserBiodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      UserBiodata.belongsTo(models.User, { foreignKey: "userId" });
    }
  }
  UserBiodata.init({
    fullname: DataTypes.STRING,
    phone: DataTypes.STRING,
    birth: DataTypes.STRING,
    address: DataTypes.STRING,
    userId: {
      type: DataTypes.INTEGER,
      unique: true,
    }
    
  }, {
    sequelize,
    modelName: 'UserBiodata',
  });
  return UserBiodata;
};