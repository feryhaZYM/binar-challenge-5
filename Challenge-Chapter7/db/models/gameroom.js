'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class GameRoom extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      GameRoom.belongsTo(models.User, { foreignKey: "playerOneId", as: "player1" });
      GameRoom.belongsTo(models.User, { foreignKey: "playerTwoId", as: "player2" });
      GameRoom.hasMany(models.GameHistory, { foreignKey: "roomId" });
    }
  }
  GameRoom.init({
    roomName: DataTypes.STRING,
    playerOneId: DataTypes.INTEGER,
    playerOneChoice: DataTypes.STRING,
    resultP1: DataTypes.STRING,
    playerTwoId: DataTypes.INTEGER,
    playerTwoChoice: DataTypes.STRING,
    resultP2: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'GameRoom',
  });
  return GameRoom;
};