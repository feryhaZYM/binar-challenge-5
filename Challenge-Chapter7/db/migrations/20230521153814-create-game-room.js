'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('GameRooms', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      roomName: {
        type: Sequelize.STRING
      },
      playerOneId: {
        type: Sequelize.INTEGER
      },
      playerOneChoice: {
        type: Sequelize.STRING
      },
      resultP1: {
        type: Sequelize.STRING
      },
      playerTwoId: {
        type: Sequelize.INTEGER
      },
      playerTwoChoice: {
        type: Sequelize.STRING
      },
      resultP2: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('GameRooms');
  }
};